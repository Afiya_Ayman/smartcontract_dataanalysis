import pandas as pd
import time
from matplotlib import pyplot as plt
from numpy import int32
from datetime import datetime
import math
import numpy as np

data_sheet = pd.read_csv('Posts_Smart_Contracts.csv', encoding='latin-1')

#nc=Number of Comments
nc=data_sheet.CommentCount

comment=0
for i in range(len(nc)):
    if nc[i]>0:
        comment+=nc[i]

#print(max(nc))
AvgCommentPerPost=comment/len(data_sheet.Id)
print(AvgCommentPerPost)

CommentCount = [0] * 11

for i in range(len(nc)):
    if (nc[i]==0):
        CommentCount[0] += 1
    if (nc[i]==1):
        CommentCount[1] += 1
    if (nc[i] == 2):
        CommentCount[2] += 1
    if (nc[i]==3):
        CommentCount[3] += 1
    if (nc[i]==4):
        CommentCount[4] += 1
    if (nc[i]==5):
        CommentCount[5] += 1
    if (nc[i]==6):
        CommentCount[6] += 1
    if (nc[i]==7):
        CommentCount[7] += 1
    if (nc[i]==8):
        CommentCount[8] += 1
    if (nc[i]==9):
        CommentCount[9] += 1
    if (nc[i] >= 10):
        CommentCount[10] += 1

print("Number of  Comments {}".format(comment))
for i in range(len(CommentCount)):
    print("{} \t\t\t\t {}".format(i, CommentCount[i]))

No_of_Comments=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10 or more']
y_pos=np.arange(len(No_of_Comments))
plt.bar(y_pos, CommentCount)
plt.xticks(y_pos, No_of_Comments)

plt.ylabel('#Posts')
plt.xlabel('#Comments')
plt.title("Posts by No of Comments")
plt.show()
