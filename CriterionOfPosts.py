import pandas as pd
import time
from matplotlib import pyplot as plt
from numpy import int32
from datetime import datetime
import math

data_sheet = pd.read_csv('Posts_Smart_Contracts.csv', encoding='latin-1')

labels = 'Poor', 'Fair', 'Good'
sizes = [0, 0, 0]
#data_sheet['AcceptedAnswerId'] = data_sheet['AcceptedAnswerId'].str.replace('NaN', '0')

count = data_sheet.AnswerCount
ac = data_sheet.AcceptedAnswerId
#print(count)
#print(ac)

for i in range(len(count)):
    if not math.isnan(ac[i]):
        sizes[2] += 1
    if math.isnan(ac[i]) and count[i] > 0:
        sizes[1] += 1
    if count[i] == 0:
        sizes[0] += 1

# Counting the number of different kind post


print("Number of  post {}".format(len(count)))
print("Number of poor post {}".format(sizes[0]))
print("Number of fair post {}".format(sizes[1]))
print("Number of good post {}".format(sizes[2]))

totalCount = len(data_sheet.Id)
sizes=[sizes[0]/totalCount, sizes[1]/totalCount, sizes[2]/totalCount]

fig1, ax1 = plt.subplots()
ax1.pie(sizes, labels=labels, autopct='%1.1f%%',
        shadow=False, startangle=110)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
plt.title(" % of Posts by Quality of Answers Provided")
plt.show()


