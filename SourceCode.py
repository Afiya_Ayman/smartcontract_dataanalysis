import pandas as pd
import time
from matplotlib import pyplot as plt
import numpy as np
from numpy import int32
from datetime import datetime

data_sheet = pd.read_csv('Posts_Smart_Contracts.csv', encoding='latin-1')
# print(data_sheet)
# print(data_sheet.CreationDate)
# print(data_sheet.dtypes)
data_sheet['CreationDate'] = pd.to_datetime(data_sheet['CreationDate'])


count18 = 0
count17 = 0
count16 = 0
count15 = 0
l = data_sheet.sort_values(by='CreationDate')


month2018 = [0] * 13
month2017 = [0] * 13
month2016 = [0] * 13
month2015 = [0] * 13

for k in l.CreationDate:
    date, time = str(k).split(" ")  # Date and time splitting

    # date parsing
    y, m, d = date.split("-")
    if y == "2018":
        m = int(m)
        month2018[m] += 1
        count18 += 1

    elif y == "2017":
        m = int(m)
        month2017[m] += 1
        count17 += 1

    elif y == "2016":
        m = int(m)
        month2016[m] += 1
        count16 += 1

    elif y == "2015":
        m = int(m)
        month2015[m] += 1
        count15 += 1

print(month2018)
print("Number of total posts in 2018 is = {}".format(count18))
#Only for month of 2018
print(" Month ", '\t', "No of Posts")     #table column headings
print("------", '\t', "-------------")
for i in range(1,len(month2018)):
    print("{} \t\t\t\t {}".format(i,month2018[i]))

print("\n\nNumber of total posts in 2017 is = {}".format(count17))
#Only for month of 2017
print(" Month ", '\t', "No of Posts")     #table column headings
print("------", '\t', "-------------")
for i in range(1,len(month2017)):
    print("{} \t\t\t\t {}".format(i,month2017[i]))

print("Number of total posts in 2016 is = {}".format(count16))
#Only for month of 2016
print(" Month ", '\t', "No of Posts")  # table column headings
print("------", '\t', "-------------")
for i in range(1,len(month2016)):
    print("{} \t\t\t\t {}".format(i,month2016[i]))

print("Number of total posts in 2015 is = {}".format(count15))
#Only for month of 2018
print("\n2015 Monthly Lists of Posts\n")
print(" Month ", '\t', "No of Posts")  # table column headings
print("------", '\t', "-------------")
for i in range(1,len(month2015)):
    print("{} \t\t\t\t {}".format(i,month2015[i]))

years=['2015', '2016', '2017', '2018']
y_pos=np.arange(len(years))
plt.bar(y_pos,[count15,count16,count17,count18])
#plt.bar(np.arange(2010., 2018, 1),[count15,count16,count17,count18],width=0.2)plt.xticks(y_pos, objects)
plt.xticks(y_pos, years)

plt.ylabel('No of Posts')
plt.title("Posts from Dec 15 - Nov 18")
plt.show()

months=['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
y_pos=np.arange(len(months))
#for i in range(len(months)):
    #A=format(month2018[i])
plt.bar(y_pos, month2018)
plt.xticks(y_pos, months)
plt.ylabel('No of Posts in 2018')
plt.title("No of Monthly Posts of 2018")
plt.show()


y_pos=np.arange(len(months))
plt.bar(y_pos, month2017)
plt.xticks(y_pos, months)
plt.ylabel('No of Posts in 2017')
plt.title("No of Monthly Posts of 2017")
plt.show()

y_pos=np.arange(len(months))
plt.bar(y_pos, month2016)
plt.xticks(y_pos, months)
plt.ylabel('No of Posts in 2016')
plt.title("No of Monthly Posts of 2016")
plt.show()

y_pos=np.arange(len(months))
plt.bar(y_pos, month2015)
plt.xticks(y_pos, months)
plt.ylabel('No of Posts in 2015')
plt.title("No of Monthly Posts of 2015")
#plt.show()









